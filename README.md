# Cémanbot

Un script NodeJS qui résout [Cémantix](https://cemantix.certitudes.org/).  

## Prérequis
- Installer [Node](https://nodejs.org/) (et npm)
- Installer les dépendances : `npm i`

## Utilisation
Lancer le script de l'une de ces manières :
```sh
node index.mjs
```
```sh
node .
```

Ou celui-ci pour la version stupide qui essaye des mots au hasard :
```sh
node index-dumb.js
```

Dans les deux cas, il est aussi possible de forcer le test d'un mot en le tapant au clavier pendant l'exécution du script puis en appuyant sur entrée.

## Crédits
Le modèle word2vec utilisé a été originellement créé par Jean-Philippe Fauconnier (http://fauconnier.github.io).  
Il s'agit de [frWac_non_lem_no_postag_no_phrase_200_cbow_cut100](https://embeddings.net/embeddings/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin).  
J'ai ensuite enlevé environ les trois quarts des vecteurs (mots) : ceux qui n'étaient pas valides pour Cémantix.
