const axios = require("axios");
const words = require("./words.json");

let bestScore = -Infinity;
const nReqMax = Math.floor(words.length / 2);
const postURL = "https://cemantix.certitudes.org/score";
const postConfig = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' }};
let cntReq = 0;
let reqDelay = 10;

function postRandomWord() {
  const randomIndex = Math.floor(Math.random() * words.length);
  const randomWord = words[randomIndex];

  postWord(randomWord, randomIndex);
}

function endSearch() {
  console.log("Finished!");
  process.stdin.removeListener("data", stdinListener);
  process.exit(0);
}

function postWord(word, index, repeat = true) {
  const params = new URLSearchParams();
  params.append("word", word);

  if (cntReq >= nReqMax) {
    endSearch();
    return;
  }
  
  cntReq++;

  axios.post(postURL, params, postConfig)
    .then(res => {
      if (res.data.hasOwnProperty("error")) {
        if (res.data.error.substring(0, 13) == "Je ne connais")
          throw new Error(`Tried invalid word '${word}'!`);
        else if (res.data.error.substring(0, 10) == "Vous tapez") {
          reqDelay += 50;
          throw new Error(`Rate-limited, changed request delay to ${reqDelay}ms!`);   
        }
      }

      if (res.status == 200 && res.data && res.data.score &&
        (res.data.score > bestScore || (res.data.percentile && res.data.percentile >= 980))) {
        
        bestScore = res.data.score;

        let mood = res.data.score < 0 ? "🧊" : "🥶";
        if (res.data.hasOwnProperty("percentile")) {
          const percentile = res.data.percentile;
          if (percentile == 1000) mood = "🥳";
          else if (percentile == 999) mood = "😱";
          else if (percentile >= 990) mood = "🔥";
          else if (percentile >= 900) mood = "🥵";
          else mood = "😎";
        }

        let dispScore = Math.round(bestScore * 10000) / 100;
        console.log(`-> ${mood} Found '${word}' with score ${dispScore}`);
      }

      if (index !== -1) words.splice(index, 1);
    })
    .catch(err => {
      console.log(`[!] ${err.message}`);
    })
    .finally(() => {
      if (bestScore >= 1) endSearch();
      if (repeat) setTimeout(postRandomWord, reqDelay);
    });
}

let currStdinData = "";
const stdinListener = d => {
  let str = d.toString();
  for (let char of str) {
    if (char == '\n' && currStdinData.trim().length > 0) {
      postWord(currStdinData, words.indexOf(currStdinData), false);
      currStdinData = "";
    } else if (char.charCodeAt(0) < 32768)
      currStdinData += char;
  }
}
process.stdin.on("data", stdinListener);

console.log("Searching for Cémantix words...");
postRandomWord();
