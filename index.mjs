import axios from "axios";
import { readFile } from "fs/promises";
import { existsSync, mkdirSync } from "fs";
import w2v from "word2vec";
import path from 'path';
import { fileURLToPath } from 'url';
import clc from "cli-color";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
process.chdir(__dirname);

function log(type, message) {
  const symbols = {
    "info": "💬",
    "warn": "😫",
    "progress": "😏",
    "fatal": "💥",
  };
  
  const symbol = symbols[type] || "❔";

  if (type === "progress")
    message = `${clc.yellow(message)} ...`
  else if (type === "fatal")
    message = clc.red(message);
  else if (type === "info")
    message = clc.cyan(message);
  let fstr = `[${symbol}] ${message}`;

  console.log(fstr);
}

function endSearch(reason) {
  let infoMessage = "Finished!";
  if (reason) infoMessage += ` (${reason})`;
  infoMessage += ` | Tries: ${tries} | Time: ${(new Date() - beginDate) / 1000}s`;
  log("info", infoMessage);

  process.stdin.removeListener("data", stdinListener);
  process.exit(0);
}

log("progress", "Loading word list");
const words = JSON.parse((await readFile("./words.json")).toString());
const w2vModelFilename = "frWac_non_lem_no_postag_no_phrase_200_cbow_cut100_filtered.bin";

const iwordTestQueue = [];
const similarsTried = [];
const similarsTested = [];
const similarsToTry = [];
let bestScore = 0;
let bestPercentile = 0;
let isTryingRandom = false;

let nReq = 0;
const nReqMax = Math.floor(words.length / 2);
const postURL = "https://cemantix.certitudes.org/score";
const postConfig = { headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Origin': 'https://cemantix.certitudes.org' }};
let reqDelay = 10;
let beginDate = new Date();
let tries = 0;

let currStdinData = "";

log("progress", "Setting up stdin reader");
const stdinListener = d => {
  let str = d.toString();
  for (let char of str) {
    if (char === '\n' && currStdinData.trim().length > 0) {
      testWord([words.indexOf(currStdinData), currStdinData]);
      currStdinData = "";
    } else if (char.charCodeAt(0) < 32768)
      currStdinData += char;
  }
}
process.stdin.on("data", stdinListener);

log("progress", "Loading word2vec model");
// Checking for the w2v model file
if (!existsSync("./word2vec"))
  mkdirSync("./word2vec");
if (!existsSync(`./word2vec/${w2vModelFilename}`)) {
  log("fatal", `Word2vec model file 'word2vec/${w2vModelFilename}' not found. Download it from the git repo before running the script.`);
  process.exit(1);
}

const model = await new Promise((resolve, reject) => {
  w2v.loadModel(`./word2vec/${w2vModelFilename}`, (err, model) => {
    if (err) reject(err);
    resolve(model);
  })
});

function pushRandomWord() {
  const i = Math.floor(Math.random() * words.length);    
  iwordTestQueue.push([i, words[i]]);
}

function pushSimilarWords(baseWordAndPercentile) {
  const [word, percentile] = baseWordAndPercentile;
  
  if (model.getVector(word) === null) {
    log("warn", `Can't find words similard to '${word}': not in word2vec model`);
    return;
  }

  log("progress", `Trying words similar to '${word}'`);
  similarsTried.push(word);

  let nSimilars = 45;
  if (percentile > 950) nSimilars = 20;
  else if (percentile > 900) nSimilars = 25;
  else if (percentile > 800) nSimilars = 30;
  else if (percentile > 500) nSimilars = 35;
  
  const similarsList = model.mostSimilar(word, nSimilars);
  
  for (const similar of similarsList)
    if (!similarsTested.includes(similar.word)) {
      iwordTestQueue.push([words.indexOf(similar.word), similar.word]);
      similarsTested.push(similar.word);
    }
}

function pushNewWords() {
  if (bestPercentile < 100 || similarsToTry.length === 0) {
    if (!isTryingRandom) {
      isTryingRandom = true;
      log("progress", "Trying random words");
    }
    pushRandomWord();
  }
  else {
    isTryingRandom = false;
    pushSimilarWords(similarsToTry.pop());
  }
}

function postWord(word) {
  if (nReq > nReqMax) endSearch("nReq exceeded");
  nReq++;

  const params = new URLSearchParams();
  params.append("word", word);
  return axios.post(postURL, params, postConfig);
}

function displayRes(word, score, percentile) {
  let mood = score < 0 ? "🧊" : "🥶";
  if (percentile) {
    if (percentile == 1000) mood = "🥳";
    else if (percentile == 999) mood = "😱";
    else if (percentile >= 990) mood = "🔥";
    else if (percentile >= 900) mood = "🥵";
    else mood = "😎";
  }

  let dispScore = Math.round(score * 10000) / 100;
  let message = `[${mood}] Found '${clc.green(word)}' | Score ${dispScore}`;
  if (percentile) message += ` | Top ${1001 - percentile}`;
  console.log(message);
}

function testWord(iword) {
  const index = iword[0], word = iword[1];

  postWord(word)
    .then(res => {
      if (res.data.hasOwnProperty("error")) {
        if (res.data.error.substring(0, 13) == "Je ne connais")
          throw new Error(`Tried invalid word '${word}'!`);
        else if (res.data.error.substring(0, 10) == "Vous tapez") {
          reqDelay += 50;
          throw new Error(`Rate-limited, changed request delay to ${reqDelay}ms!`);
        }
      }

      if (res.status == 200 && res.data && res.data.score) {
        tries++;

        if (res.data.score > bestScore)
          bestScore = res.data.score;

        if (res.data?.percentile > bestPercentile)
          bestPercentile = res.data.percentile;

        if (res.data.score == bestScore || res.data?.percentile > 965) {
          displayRes(word, res.data.score, res.data?.percentile);
          if (res.data?.percentile >= 100 && !similarsTried.includes(word)) {
            similarsToTry.push([word, res.data?.percentile]);
            similarsToTry.sort((a, b) => a[1] - b[1]);
          }
        }
      }

      if (index !== -1) words.splice(index, 1);
    })
    .catch(err => {
      log("warn", err.message);
    })
    .finally(() => {
      if (bestPercentile >= 1000) endSearch("word found");
    });
}

function regularTest(repeat = true) {
  if (iwordTestQueue.length == 0)
    pushNewWords();
  const iword = iwordTestQueue.shift();

  if (iword) testWord(iword);
  if (repeat) setTimeout(regularTest, reqDelay);
}

log("progress", "Word search begins");
beginDate = new Date();
regularTest();
