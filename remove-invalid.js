const axios = require("axios");
const words = require("./words.json");
const fs = require("fs");

const postURL = "https://cemantix.certitudes.org/score";
const postConfig = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' }};

let initialLength = words.length;
let currIndex = words.length - 1;

function log(message) {
  const percentage = ((initialLength - currIndex) / initialLength * 100).toFixed(2);
  console.log(`[${percentage}%] ${message}`);
}

function saveWords() {
  log("Saving words to new-words.json");
  fs.writeFileSync("./new-words.json", JSON.stringify(words));
}

function testCurrWord() {
  const word = words[currIndex];
  const params = new URLSearchParams();
  params.append("word", word);

  axios.post(postURL, params, postConfig)
    .then(res => {
      if (res.status == 200 && res.data && res.data.hasOwnProperty("error")) {
        if (res.data.error.substring(0, 13) == "Je ne connais") {
          words.splice(currIndex, 1);
          log(`Removed word '${word}', new length ${words.length}`);
        }
        else if (res.data.error.substring(0, 10) == "Vous tapez")
          throw new Error("Rate-limited!");
      }

      currIndex--;
    })
    .catch(err => {
      console.log(`[!] ${err.message}`);
    })
    .finally(() => {
      if (currIndex >= 0) setTimeout(testCurrWord, 10);
      else {
        console.log("Finished!");
        saveWords();
      }
    });
}

setInterval(saveWords, 300000);

console.log("Searching for invalid Cémantix words...");
testCurrWord();
